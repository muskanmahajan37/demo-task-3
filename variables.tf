variable "ssh_key" {
  default = ""
}
variable "DB_PASS" {
  default = ""
}
variable "DB_NAME" {
  default = ""
}
variable "DB_USERNAME" {
  default = ""
}
