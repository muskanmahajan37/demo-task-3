packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.1"
      source  = "github.com/hashicorp/amazon"
    }
  }
}
 
source "amazon-ebs" "petclinic_app_ami" {
  ami_name      = "petclinic_app_ami"
  instance_type = "t2.micro"
  region        = "us-east-2"
  source_ami_filter {
    filters = {
      name                = "*ubuntu-focal-20.04-amd64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }
  ssh_username = "ubuntu"
  force_deregister            = true
  force_delete_snapshot       = true
}

build {
  sources = [
    "source.amazon-ebs.petclinic_app_ami"
  ]
  provisioner "shell" {
      inline = [
          "mkdir -p /home/ubuntu/petclinic-app; chmod -R a+rw /home/ubuntu/petclinic-app",
          "curl -fsSL get.docker.com -o docker.sh; chmod +x docker.sh; sudo ./docker.sh; sudo systemctl start docker.service; sudo systemctl enable docker.service",
          "sudo curl -L https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/bin/docker-compose; sudo chmod +x /usr/bin/docker-compose"
      ]
  }
}
