FROM adoptopenjdk/openjdk11:x86_64-alpine-jre-11.0.10_9
COPY app.jar .
EXPOSE 8080
CMD java -jar app.jar

