resource "aws_key_pair" "ec2_keys" {
  key_name   = "ec2_keys"
  public_key = var.ssh_key
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["petclinic_app_ami"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["218294504414"]
}

resource "aws_instance" "petclinic_app" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.petclinic_app_subnet.id
  vpc_security_group_ids = [aws_security_group.petclinic_app_sg.id]
  key_name               = "ec2_keys"
  root_block_device {
    volume_size = "30"
  }
  depends_on = [aws_db_instance.petclinic_db]

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("/home/gitlab-runner/.ssh/id_rsa")
    host        = aws_instance.petclinic_app.public_ip
  }

  provisioner "local-exec" {
    command = "export DB_ADDRESS=${aws_db_instance.petclinic_db.address}; chmod +x env.sh; ./env.sh"
  }

  provisioner "file" {
    source      = "env_vars"
    destination = "/home/ubuntu/petclinic-app/env_vars"
  }
  provisioner "file" {
    source      = "petclinic-app.dockerfile"
    destination = "/home/ubuntu/petclinic-app/petclinic-app.dockerfile"
  }
  provisioner "file" {
    source      = "app-compose.yml"
    destination = "/home/ubuntu/petclinic-app/app-compose.yml"
  }

  provisioner "file" {
    source      = "/home/gitlab-runner/app.jar"
    destination = "/home/ubuntu/petclinic-app/app.jar"
  }

  provisioner "remote-exec" {
    inline = [
      "cd /home/ubuntu/petclinic-app; sudo docker-compose -f app-compose.yml up --detach --build"
    ]

  }

  tags = {
    Name  = "petclinic_app"
    Owner = "NicatB"
  }
}
